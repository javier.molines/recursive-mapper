const getDataByBackendMs = (data: any, key: string) => {
	const nameDefault = "Not Found";

	// Si no es recursiva, directamente retorna el item
	if (!/\./gi.test(key)) {
		return data[key] ?? nameDefault;
	}

	let objPartials: any = {};
	const splitMapper = key.split(".").filter((item) => item !== "");

	// Busca de forma recursiva el item hasta que termina
	for (const query of splitMapper) {
		const withPosition = /[\d]/gi.test(query);
		const withData = Object.entries(objPartials).length === 0;
		const getMapperPartials = withPosition
			? objPartials[query.replace(/\[|\]/gi, "")]
			: objPartials[query];

		objPartials = withData ? data[query] : getMapperPartials;

		if (!objPartials) {
			return nameDefault;
		}
	}

	return objPartials;
};

export { getDataByBackendMs };
