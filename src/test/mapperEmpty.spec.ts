import { describe, expect, it } from "vitest";
import { getDataByBackendMs } from "../app";
import { mockResponseEmpty } from "./mock";

describe(">>> Object not found, always not found return", () => {
	const notValid = "Not Found";

	it("> item not exists in dataResponse", () => {
		expect(getDataByBackendMs(mockResponseEmpty, "item")).toBe(notValid);
	});

	it("> item.pass not exists in dataResponse", () => {
		expect(getDataByBackendMs(mockResponseEmpty, "item.pass")).toBe(notValid);
	});

	it("> item.pass withs more .... not exists in dataResponse", () => {
		expect(getDataByBackendMs(mockResponseEmpty, "item.pass...")).toBe(
			notValid,
		);
	});

	it("> example.generic not exists in dataResponse", () => {
		expect(getDataByBackendMs(mockResponseEmpty, "example.generic")).toBe(
			notValid,
		);
	});

	it("> results.[0] not exists in dataResponse", () => {
		expect(getDataByBackendMs(mockResponseEmpty, "results.[0]")).toBe(notValid);
	});

	it("> results.[0].example not exists in dataResponse", () => {
		expect(getDataByBackendMs(mockResponseEmpty, "results.[0].example")).toBe(
			notValid,
		);
	});

	it("> results.[0].location.street.works.[0] not exists in dataResponse", () => {
		expect(
			getDataByBackendMs(
				mockResponseEmpty,
				"results.[0].location.street.works.[1]",
			),
		).toBe(notValid);
	});
});
