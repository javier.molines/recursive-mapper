import { describe, expect, it } from "vitest";
import { getDataByBackendMs } from "../app";
import { mockResponseData } from "./mock";

describe(">>> Object found", () => {
	const models = [
		{
			index: "info.page",
			result: 1,
		},
		{
			index: "info.seed",
			result: "56d27f4a53bd5441",
		},
		{
			index: "results.[0].gender",
			result: "female",
		},
		{
			index: "results.[0].login.username",
			result: "yellowpeacock117",
		},
		{
			index: "results.[0].location.street.name",
			result: "Valwood Pkwy",
		},
		{
			index: "results.[0].location.street.works.[0]",
			result: "OPEN",
		},
		{
			index: "results.[0].location.street.works.[1].vip",
			result: true,
		},
	];

	for (const { index, result } of models) {
		it(`> ${index} exists in dataResponse`, () => {
			expect(getDataByBackendMs(mockResponseData, index)).toBe(result);
		});
	}
});
